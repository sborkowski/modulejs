# Module.js

Module.js is a client-side version of Node.js' module loader. It uses the same paradigm for loading JavaScript files as Node.js, with a few tweaks. This does use XHR so be mindful of how you load scripts.

## Require

`require(id)` expects a path(id) to a JavaScript file containing a module.

There are 3 ways to specify a path:

###Root

By not using any leading slash, the path will resolve to the domain root. This is similar to how NodeJS would look for a core module or a module in the 'node_modules' folder. In this case, it's a bit different because there is no notion of core modules.

####index.html
	<script type="text/javascript" src="/modules/myModule.js"></script>

####myModule.js
	require("/root_modules/secondModule.js"); // "http://domain.com/root_modules/myModule.js"
	
###Local
	
Using a leading slash means the path is relative to the script that called it. If it was a linked script, the path will resolve relative to that script's location.

####index.html
	<script type="text/javascript" src="/modules/myModule.js"></script>
	
####myModule.js
	require("/subfolder/secondModule.js"); // "http://domain.com/modules/subfolder/secondModule.js"

####secondModule.js
	require("/thirdModule.js"); // "http://domain.com/modules/thirdModule.js"
	
###Relative

Using a single and a slash makes the path relative to the script or module that loaded it. Using double dots moves up a folder.

####index.html
	
	<script type="text/javascript" src="/modules/myModule.js"></script>
	
	
####myModule.js
	
	require("./modules/secondModule.js"); // "http://domain.com/modules/secondModule.js"
	
## Module

Modules work the same as they do in Node.js. Each module has its own context, but in order to access it, you must declare your variables with `var`, otherwise they will be added to the Global scope.

####myModule.js

	// Visible only to the module
	var localVar = {};
	
	globalVar = {};

## Exporting
	
To export local variables, we can use the `module` object and it's `exports` property.

####myModule.js

	// Visible only to the module
	var localVar = {};
	
	module.exports = localVar;

####index.html
	<script type="text/javascript">
		var myModule = require("./myModule.js");
	</script>

	