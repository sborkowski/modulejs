/*
 *  CC0 1.0 Universal - Public Domain Dedication:
 *	http://creativecommons.org/publicdomain/zero/1.0/
 *
 */


function require(id)
{
	var re = require;
	if (!re.cache) re.cache = {};
	if (!re.stack) re.stack = [];
	
	var src = null, dir = [], path = id.split('/');
	
	if (path[0] == "")
		src = window.location.origin;
	else if (!re.stack.length)
	{
		var s = document.getElementsByTagName('script');
		s = s[s.length-1].src;
		if (s != "")
			src = s.split('/').slice(0, -1).join('/');
		else
			src = window.location.href;
	}
	else if (path[0] == "." || path[0] == "..")
		src = re.stack[re.stack.length-1];

	while (path.length > 1)
	{
		var p = path.shift();
		if (p == "..")
		{
			if (src.length > 1)
				src = src.split('/').slice(0, -1).join('/');
			else
				throw "Error: Path resolves beyond root.";
		}
		else if (p != "" && p != ".")
			dir.push(p);
	}
	
	id = src + "/" + dir.concat(path).join("/");
	
	console.log("Loading module " + id);
	
	if (!re.cache[id])
	{
		
		var r = new XMLHttpRequest();
		r.open("GET", id, false);
		r.send();
	
		if (r.status === 200 || r.status === 0)
		{
			re.cache[id] = {
				"id" : id,
				"filename" : path[0]
			};
			re.stack.push([src].concat(dir).join("/"));
			try
			{
				re.cache[id].exports = (new Function(
					"\nvar module = { id: this.id, filename: this.filename, exports: {} };\n" + r.responseText + "\nreturn module.exports;\n\n//# sourceURL=" + id)
				).call(re.cache[id]);
			}
			finally
			{
				re.stack.pop();
			}
			return re.cache[id].exports;
		};
	}
	else
		return re.cache[id].exports;
};
